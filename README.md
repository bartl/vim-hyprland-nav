# vim-hyprland-nav

Seamless navigation between [Hyprland](https://hyprland.org/) windows and (Neo)Vim splits with the same key bindings.

Inspired by and a lot of code borrowed from [vim-sway-nav](https://jasoncarloscox.com/creations/vim-sway-nav/).

## Requirements

- Vim built with `+clientserver` (check with `vim --version`), or Neovim
- [jq](https://github.com/stedolan/jq)

## Installation

First, install this repository as a (neo)vim plugin. For example, if you use [Lazy](https://github.com/folke/lazy.nvim):

```
{
  url = "https://gitlab.com/bartl/vim-hyprland-nav",
}
```

Second, download the `vim-hyprland-nav` script and stick it somewhere in your `$PATH` (or use the full path in your hyprland config below)

Finally, modify your hyprland config to use `vim-hyprland-nav` instead of your normal `movefocus l`, `movefocus d`, etc. bindings:

```
bind = $mainMod, h, exec, vim-hyprland-nav left
bind = $mainMod, j, exec, vim-hyprland-nav down
bind = $mainMod, k, exec, vim-hyprland-nav up
bind = $mainMod, l, exec, vim-hyprland-nav right
```

You can now use `$mainMod+<h,j,k,l` to navigate among Hyprland windows and Vim splits!

## Configuration
There are two methods in which the plugin detects if the Hyprland window is running (neo)vim or not.

You can switch between the two methods by setting the `VIM_HYPRLAND_NAV_METHOD` environment variable.

In Hyprland you can do this with

`env VIM_HYPRLAND_NAV_METHOD,method`

### File-based detection
The original way, based on vim-sway-nav. This is very reliable, but might be a bit slow sometimes.
This is the default method. You can explicitly enable it by setting `VIM_HYPRLAND_NAV_METHOD` to `file`

### Title-based detection
Most of the time this is faster, but it has some drawbacks:

- Not every terminal program might support it (I have only tested it with [foot](https://codeberg.org/dnkl/foot))
- Depends on [title](https://vimhelp.org/options.txt.html#%27title%27) and [titlestring](https://vimhelp.org/options.txt.html#%27titlestring%27), so if you, or any other plugin you use, modify these settings, this will not work.

You can enable this method by setting `VIM_HYPRLAND_NAV_METHOD` to `title`

## Contributing

Contributions are welcome! Use the [issue tracker](https://gitlab.com/bartl/vim-hyprland-nav/-/issues) to report bugs, request features, etc...
Pull requests are welcome!
